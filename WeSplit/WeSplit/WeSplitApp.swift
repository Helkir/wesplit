//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Arnaud Chrétien on 26/09/2021.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
